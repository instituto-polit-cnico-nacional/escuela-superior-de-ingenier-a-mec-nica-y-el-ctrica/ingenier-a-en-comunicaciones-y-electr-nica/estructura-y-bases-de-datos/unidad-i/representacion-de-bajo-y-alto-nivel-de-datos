import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
        EjercicioPropuestos ejercicios = new EjercicioPropuestos();
       // ejercicios.cincoEnterosYTresReales();
       // ejercicios.calcularCircunferencia();
        ejercicios.programaQueLeaLaBYADeTrapecio();
	}
}

class EjercicioPropuestos {
    static Scanner sc = new Scanner(System.in);
    static final double PI=3.1415;

	 public void cincoEnterosYTresReales(){
	 System.out.print("Ingrese 1er numero: ");
        int numero1 = sc.nextInt();
        System.out.print("Ingrese 2do numero: ");
        int numero2 = sc.nextInt();
        System.out.print("Ingrese 3er numero: ");
        int numero3 = sc.nextInt();
        System.out.print("Ingrese 4to numero: ");
        int numero4 = sc.nextInt();
        System.out.print("Ingrese 5to numero: ");
        int numero5 = sc.nextInt();
        System.out.print("Ingrese 1er numero decimal: ");
        double decimal1 = sc.nextDouble();
        System.out.print("Ingrese 2do numero decimal: ");
        double decimal2 = sc.nextDouble();
        System.out.print("Ingrese 3ro numero decimal: ");
        double decimal3 = sc.nextDouble();
        // Imprima los valores para verificar si la entrada
        // fue obtenida correctamente.
        System.out.println("Tus numeros obtenidos son: "+numero1+","+numero2+","+numero3+","+numero4+","+numero5);
        System.out.println("Tus numeros decimales obtenidos son: "+decimal1+","+decimal2+","+decimal3);
    }
    public void calcularCircunferencia(){
	 System.out.print("Ingrese el radio: ");
        double radio = sc.nextDouble();
        double circunferencia= 2*PI*radio;
        System.out.println("La circunferencia es: "+circunferencia);
    }

    public void programaQueLeaLaBYADeTrapecio(){
	 System.out.print("Ingrese la base mayor del trapecio: ");
        double baseMay = sc.nextDouble();
        System.out.print("Ingrese la base menor del trapecio: ");
        double baseMen = sc.nextDouble();
        System.out.print("Ingrese la altura del trapecio: ");
        double altura= sc.nextDouble();
        double areadeltrapedio=((baseMay+baseMen)*altura)/2;
        System.out.println("La area del trapecio es: "+areadeltrapedio);
    }

}
