# UNIDAD I: Representación de bajo y alto nivel de datos

## OBJETIVOS PARTICULARES DE LA UNIDAD

El alumno distinguirá los diferentes tipos de datos que se pueden manejar en una computadora, así como su representación interna.

| No. Tema  | Título Tema                      | Teoría              | Práctica            | Evaluación Continua  | Clave Bibliográfica  |
| :---      | :----                            | :---:               | :---:               | :---:                | :---:                |
| 1.1       | REPRESENTACIÓN DE BAJO NIVEL     | :white_check_mark:  | :white_check_mark:  | :white_check_mark:   | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |
| 1.1.1     | Constantes enteras               | :white_check_mark:  | :white_check_mark:  | :x:                  | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |
| 1.1.2     | Constantes reales                | :white_check_mark:  | :white_check_mark:  | :x:                  | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |
| 1.1.3     | Constantes caracter / cadena     | :white_check_mark:  | :white_check_mark:  | :x:                  | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |
| 1.2       | REPRESENTACIÓN DE ALTO NIVEL     | :white_check_mark:  | :white_check_mark:  | :white_check_mark:   | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |
| 1.2.1     | Constantes enteras               | :white_check_mark:  | :white_check_mark:  | :x:                  | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |
| 1.2.2     | Constantes reales                | :white_check_mark:  | :white_check_mark:  | :x:                  | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |
| 1.2.3     | Constantes caracter              | :white_check_mark:  | :white_check_mark:  | :x:                  | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |
|           | Horas Totales                    | 2.0                 | 3.0                 | 2.0                  | [2B, 9C](activos/pdfs/UNIDAD_I-Representacion_de_bajo_y_alto_nivel_de_datos.pdf)  |

## ESTRATEGIA DIDÁCTICA

El alumno investigará las representaciones de bajo y alto nivel de los tipos primitivos de datos en una computadora, y se discutirán en clase.
Efectuará programas en los cuales sature la memoria de los diferentes tipos de datos.

## PROCEDIMIENTO DE EVALUACIÓN

* Programas y ejercicios desarrollados en clase y extra clase.
* Examen del periodo.
